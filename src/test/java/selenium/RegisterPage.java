package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {

    protected static WebDriver webDriver;
    private By pathRegisterB = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div");
    private By pathVorname = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[1]/div/input");
    private By pathNachname = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[3]/div/input");
    private By pathEmail = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[5]/div/input");
    private By pathPassword = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[7]/div/input");
    private By pathRegisterButton = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[9]/div");

    public RegisterPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void register(String email, String vorname, String nachname, String password) {
        webDriver.findElement(pathRegisterB).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 2);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pathVorname));
        webDriver.findElement(pathVorname).sendKeys(vorname);
        webDriver.findElement(pathNachname).sendKeys(nachname);
        webDriver.findElement(pathEmail).sendKeys(email);
        webDriver.findElement(pathPassword).sendKeys(password);
        webDriver.findElement(pathRegisterButton).click();
    }
}
