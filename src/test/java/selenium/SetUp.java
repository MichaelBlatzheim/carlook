package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SetUp {

    private static WebDriver webDriver = null;

    private SetUp() {

    }

    public static WebDriver getWebDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Toshiba\\IdeaProjects\\carlook\\chromedriver.exe");


        if (webDriver == null) {
            webDriver = new ChromeDriver();
        }

        webDriver.get("http://localhost:8080/");
        webDriver.manage().window().fullscreen();

        return webDriver;


    }

}
