package selenium;

import org.bonn.model.dao.KundeDAO;
import org.bonn.model.entities.Kunde;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.sql.SQLException;

import static org.junit.Assert.assertTrue;

public class TestPage {

    private static WebDriver webDriver = null;

    @Test
    public void test() throws SQLException {
        assertTrue(testRegister());
        assertTrue(testSignIn());
        webDriver.quit();
    }

    public boolean testSignIn() throws SQLException {

        webDriver = SetUp.getWebDriver();

        SignInPage signInPage = new SignInPage(webDriver);
        signInPage.loginValidUser("tester@gmx.de", "test");
        KundeDAO kundeDAO = new KundeDAO();
        return kundeDAO.delete(kundeDAO.getUserByEmailAndPass("tester@gmx.de", "test"));

    }

    public boolean testRegister() throws SQLException{

        webDriver = SetUp.getWebDriver();

        RegisterPage registerPage = new RegisterPage(webDriver);
        registerPage.register("tester@gmx.de", "Michael", "Müller", "test");

        KundeDAO kundeDAO = new KundeDAO();

        return (kundeDAO.getUserByEmailAndPass("tester@gmx.de","test").getEmail().equals("tester@gmx.de"));



    }


}
