package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPage {

    protected static WebDriver webDriver;
    private By pathLoginB = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[1]/div");
    private By pathUserName = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[1]/div/input");
    private By pathPassword = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[3]/div/input");
    private By pathLoginButton = By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div/div[5]/div");
    private By pathLogoutButton = By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div/div/div[5]/span/span[2]");

    public SignInPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public boolean loginValidUser(String userName, String password){
        webDriver.findElement(pathLoginB).click();
        WebDriverWait waitBefore = new WebDriverWait(webDriver, 2);
        waitBefore.until(ExpectedConditions.visibilityOfElementLocated(pathUserName));
        webDriver.findElement(pathUserName).sendKeys(userName);
        webDriver.findElement(pathPassword).sendKeys(password);
        webDriver.findElement(pathLoginButton).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 2);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pathLogoutButton));
        webDriver.findElement(pathLogoutButton).click();

        return true;
    }


}
