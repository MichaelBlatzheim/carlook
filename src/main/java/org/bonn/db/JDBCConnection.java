package org.bonn.db;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCConnection {


    private static JDBCConnection connection = null;

    private String userName = "mblatz2s";
    private String userPass = "mblatz2s";
    private String url = "jdbc:postgresql://dumbo.inf.h-brs.de:5432/mblatz2s";
    private Connection con;

    private JDBCConnection() {
        this.initConnection();
    }

    public static JDBCConnection getInstance() {
        if (JDBCConnection.connection == null) {
            JDBCConnection.connection = new JDBCConnection();
        }
        return JDBCConnection.connection;
    }

    public void initConnection() {
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
        } catch (SQLException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.openConnection();
    }

    public void openConnection() {
        try {
            this.con = DriverManager.getConnection(this.url, userName, userPass);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PreparedStatement getPreparedStatement(String sql) {
        try {
            if (this.con.isClosed()) {
                this.openConnection();
            }
            return this.con.prepareStatement(sql);

        } catch (SQLException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }


    public Statement getStatement() { //Umsetzung mit Statement als Rückgabewert
        try {
            if (this.con.isClosed()) {
                this.openConnection();
            }
            return this.con.createStatement();

        } catch (SQLException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void closeConnection() {
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public ResultSet getResultSet(String sql) throws NullPointerException {
        try {
            if (this.con.isClosed()) {
                this.openConnection();
            }
            return this.con.createStatement().executeQuery(sql);

        } catch (SQLException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}

