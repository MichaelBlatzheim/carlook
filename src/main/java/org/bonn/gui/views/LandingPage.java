package org.bonn.gui.views;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.gui.views.kunde.KundeRegistrationView;


public class LandingPage extends VerticalLayout {

    public LandingPage() {
        addStyleName("loginview");
        setSizeFull();
        setMargin(false);
        setSpacing(false);

        // Buttons für Navigation
        Button login = new Button("Login", (Button.ClickListener) event ->
            UI.getCurrent().setContent(new LoginView()));

        Button regist = new Button("Registrieren", (Button.ClickListener) event ->
            UI.getCurrent().setContent(new KundeRegistrationView())
        );
        login.setIcon(VaadinIcons.USER);
        regist.setIcon(VaadinIcons.ARROW_FORWARD);

        login.setStyleName(ValoTheme.BUTTON_FRIENDLY);


        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(login);
        horizontalLayout.addComponent(regist);
        addComponent(horizontalLayout);
        setComponentAlignment(horizontalLayout, Alignment.MIDDLE_CENTER);
        setExpandRatio(horizontalLayout, 4);
    }

}
