package org.bonn.gui.views;

import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;

public class MainView extends HorizontalLayout {
    public MainView() {
        setSizeFull();
        setSpacing(false);
        addStyleName("mainview");
        // Linkes NavigationMenü
        addComponent(new NavigationMenu());

        ComponentContainer content = new CssLayout();
        content.addStyleName("view-content");
        content.setSizeFull();
        addComponent(content);
        setExpandRatio(content, 1.0f);

        // Rechter Inhalt der Angezeigt wird
        new ContentNavigator(content);
    }

}
