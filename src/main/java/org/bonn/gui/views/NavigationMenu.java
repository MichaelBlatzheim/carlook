package org.bonn.gui.views;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.model.dto.KundeDTO;
import org.bonn.process.control.LoginController;


public class NavigationMenu extends CustomComponent {

    private static final String STYLE_VISIBLE = "valo-menu-visible";
    private final LoginController loginController = new LoginController();
    private MenuBar.MenuItem settingsItem;

    public NavigationMenu() {
        setPrimaryStyleName("valo-menu");
        setSizeUndefined();
        setCompositionRoot(buildContent());
    }

    private Component buildContent() {
        final CssLayout menuContent = new CssLayout();

        menuContent.addStyleName("sidebar");
        menuContent.addStyleName(ValoTheme.MENU_PART);
        menuContent.addStyleName("noHints");
        menuContent.setWidth(null);
        menuContent.setHeight("100%");
        menuContent.addComponent(buildTitle());
        menuContent.addComponent(buildUserMenu());
        menuContent.addComponent(buildToggleButton());
        menuContent.addComponent(buildUserMenuItems());

        final Button buttonLogout = new Button("Logout", (Button.ClickListener) event ->
            loginController.logout());

        buttonLogout.setPrimaryStyleName("valo-menu-item");
        buttonLogout.setIcon(FontAwesome.SIGN_OUT);

        menuContent.addComponent(buttonLogout);

        return menuContent;
    }

    private Component buildTitle() {
        Label logo = new Label("CarLook <strong>User-Menu</strong>",
                ContentMode.HTML);
        logo.setSizeUndefined();
        HorizontalLayout titel = new HorizontalLayout(logo);
        titel.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
        titel.addStyleName("valo-menu-title");
        titel.setSpacing(false);
        return titel;
    }

    private Component buildUserMenu() {
        final MenuBar settings = new MenuBar();
        settings.addStyleName("user-menu");

        settingsItem = settings.addItem("",
                new ThemeResource("img/profile-pic-300px.jpg"), null);


        if (loginController.isKunde()) {
            KundeDTO kundeDTO = (KundeDTO) UI.getCurrent().getSession().getAttribute("currentUser");
            settingsItem.setText(kundeDTO.getVorname() + " " + kundeDTO.getNachname());
        }

        return settings;
    }

    //Nur Mobil
    private Component buildToggleButton() {
        Button valoMenuToggleButton = new Button("Menu", (Button.ClickListener) event -> {
            if (getCompositionRoot().getStyleName()
                    .contains(NavigationMenu.STYLE_VISIBLE)) {
                getCompositionRoot().removeStyleName(NavigationMenu.STYLE_VISIBLE);
            } else {
                getCompositionRoot().addStyleName(NavigationMenu.STYLE_VISIBLE);
            }
        });
        valoMenuToggleButton.setIcon(FontAwesome.LIST);
        valoMenuToggleButton.addStyleName("valo-menu-toggle");
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
        return valoMenuToggleButton;
    }

    private Component buildUserMenuItems() {
        CssLayout userItemsLayout = new CssLayout();
        userItemsLayout.addStyleName("valo-menuitems");
        if (loginController.isKunde()) {
            for (final KundeViewType view : KundeViewType.values()) {
                Component userMenuItem = new StudentMenuItemButton(view);
                userItemsLayout.addComponent(userMenuItem);
            }
        }

        return userItemsLayout;
    }

    public static final class StudentMenuItemButton extends Button {

        public StudentMenuItemButton(final KundeViewType view) {
            setPrimaryStyleName("valo-menu-item");
            setIcon(view.getIcon());
            setCaption(view.getViewName().substring(0, 1).toUpperCase()
                    + view.getViewName().substring(1));
            addClickListener((ClickListener) event -> UI.getCurrent().getNavigator()
                    .navigateTo(view.getViewName()));
        }
    }


}
