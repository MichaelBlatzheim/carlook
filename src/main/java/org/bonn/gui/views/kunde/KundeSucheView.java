package org.bonn.gui.views.kunde;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.component.AutoSucheView;
import org.bonn.gui.views.ProfilHeader;
import org.bonn.model.entities.Auto;
import org.bonn.process.control.AutoController;

import java.util.Set;

public class KundeSucheView extends VerticalLayout implements View {
    AutoController gc = new AutoController();
    VerticalLayout vl = new VerticalLayout();
    ProfilHeader header = new ProfilHeader();

    public KundeSucheView() {
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        addComponent(header.buildHeader("Suche für Neu- und Gebrauchtwagen"));
        Component content = buildcontent();
        addComponent(content);
        setExpandRatio(content, 1);
    }

    private Component buildcontent() {
        Grid<Auto> grid = new Grid<>();

        Set<Auto> gesuche = gc.getAllGesuche();
        //TODO try catch
        ListDataProvider<Auto> dataProvider = DataProvider.ofCollection(gesuche);
        grid.setDataProvider(dataProvider);
        grid.addColumn(Auto::getMarke).setCaption("Marke");
        grid.addColumn(Auto::getBaujahr).setCaption("Baujahr");
        grid.addColumn(Auto::getBeschreibung).setCaption("Beschreibung");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.setSizeFull();
        grid.asSingleSelect();
        grid.addItemClickListener(event ->
                UI.getCurrent().addWindow(new AutoSucheView(event.getItem())));

        final TextField filter = new TextField("Suche");
        filter.setPlaceholder("Suchbegriff");

        filter.addValueChangeListener(event -> {


            try {
                dataProvider.setFilter(Auto::getBaujahr, titel -> titel.equals(Integer.valueOf(event.getValue())));
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }


            dataProvider.setFilter(Auto::getMarke, titel -> {
                String nameLower = titel == null ? ""
                        : titel.toLowerCase();
                String filterLower = event.getValue()
                        .toLowerCase();
                return nameLower.contains(filterLower);
            });

        });

        vl.addComponent(filter);
        vl.addComponent(grid);
        return vl;
    }
}
