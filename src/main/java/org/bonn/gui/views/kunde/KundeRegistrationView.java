package org.bonn.gui.views.kunde;

import com.vaadin.annotations.Title;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.exception.EmailVorhanden;
import org.bonn.gui.views.LandingPage;
import org.bonn.model.dto.KundeDTO;
import org.bonn.process.control.RegisterController;

import java.sql.SQLException;

@Title("Kunde - Registrierung")
@PushStateNavigation
public class KundeRegistrationView extends VerticalLayout {

    private final RegisterController registerController = new RegisterController();
    KundeDTO kundeDTO = new KundeDTO();

    public KundeRegistrationView() {
        addStyleName("loginview");
        setSizeFull();
        setMargin(false);
        setSpacing(false);

        Component registrationForm = buildRegistrationForm();
        addComponent(registrationForm);
        setComponentAlignment(registrationForm, Alignment.MIDDLE_CENTER);
    }

    private Component buildRegistrationForm() {
        final VerticalLayout registrationPanel = new VerticalLayout();
        registrationPanel.setSizeUndefined();
        registrationPanel.setMargin(false);
        Responsive.makeResponsive(registrationPanel);
        registrationPanel.addStyleName("login-panel");

        registrationPanel.addComponent(buildLabels());
        registrationPanel.addComponent(buildContent());
        return registrationPanel;
    }

    private Component buildLabels() {
        CssLayout labels = new CssLayout();
        labels.addStyleName("labels");

        Label welcome = new Label("Benutzerkonto anlegen");
        welcome.setSizeUndefined();
        welcome.addStyleName(ValoTheme.LABEL_H4);
        welcome.addStyleName(ValoTheme.LABEL_COLORED);
        labels.addComponent(welcome);
        welcome.setWidthFull();

        return labels;


    }

    private Component buildContent() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addStyleName("fields");

        Binder<KundeDTO> kundeDTOBinder = new Binder<>();

        TextField fieldNachname = new TextField("Nachname");
        kundeDTOBinder.forField(fieldNachname)
                .asRequired("Bitte geben Sie ihren Nachnamen ein")
                .bind(
                        KundeDTO::getNachname,
                        KundeDTO::setNachname);
        fieldNachname.setId("nachname");
        fieldNachname.setWidth("100%");
        fieldNachname.setIcon(FontAwesome.USER);
        fieldNachname.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);


        TextField fieldVorname = new TextField("Vorname");
        kundeDTOBinder.forField(fieldVorname)
                .asRequired("Bitte geben Sie Ihren Vornamen ein")
                .bind(KundeDTO::getVorname, KundeDTO::setVorname);
        fieldVorname.setId("vorname");
        fieldVorname.setWidth("100%");
        fieldVorname.setIcon(FontAwesome.USER);
        fieldVorname.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        TextField fieldEmail = new TextField("E-Mail");
        kundeDTOBinder.forField(fieldEmail)
                .withValidator(new EmailValidator("Das sieht nicht nach einer E-Mail aus"))
                .asRequired("Bitte geben Sie eine E-Mail Addresse ein.")
                .bind(KundeDTO::getEmail, KundeDTO::setEmail);
        fieldEmail.setId("email");
        fieldEmail.setWidth("100%");
        fieldEmail.setIcon(FontAwesome.USER);
        fieldEmail.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);


        PasswordField fieldpass = new PasswordField("Passwort");
        kundeDTOBinder.forField(fieldpass).asRequired().bind(KundeDTO::getPasswort, KundeDTO::setPasswort);
        fieldpass.setId("pass");

        final Button buttonRegister = new Button("Registrieren", (Button.ClickListener) event -> {
            try {
                kundeDTOBinder.writeBean(kundeDTO);
            } catch (ValidationException e) {
                e.printStackTrace();
            }

            try {
                if (registerController.registerKunde(kundeDTO)) {
                    UI.getCurrent().setContent(new LandingPage());
                    // UI.getCurrent().setContent(new LandingPage());//TODO alt
                    Notification success = new Notification("Ihre Registrierung ist erfolgt!");
                    success.setDelayMsec(6000);
                    success.setStyleName("bar success small");
                    success.setPosition(Position.BOTTOM_CENTER);
                    success.show(Page.getCurrent());
                } else {
                    Notification.show("Ein Fehler ist aufgetreten. Versuchen sie es später noch einmal.",
                            Notification.Type.ERROR_MESSAGE);
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (EmailVorhanden emailVorhanden) {
                Notification.show(emailVorhanden.getMessage(),
                        Notification.Type.ERROR_MESSAGE);
                emailVorhanden.printStackTrace();
            }
        });

        final Button buttonAbbrechen = new Button("Abbrechen", (Button.ClickListener) event ->
            UI.getCurrent().getPage().reload());

        buttonRegister.addStyleName(ValoTheme.BUTTON_PRIMARY);
        buttonAbbrechen.addStyleName(ValoTheme.BUTTON_QUIET);
        buttonRegister.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        buttonRegister.focus();


        verticalLayout.addComponents(
                fieldVorname
                , fieldNachname
                , fieldEmail
                , fieldpass
                , buttonRegister
                , buttonAbbrechen
        );
        verticalLayout.setComponentAlignment(buttonRegister, Alignment.BOTTOM_LEFT);
        verticalLayout.setComponentAlignment(buttonAbbrechen, Alignment.BOTTOM_LEFT);
        return verticalLayout;
    }
}
