package org.bonn.gui.views.kunde;

import com.vaadin.data.Binder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.gui.views.ProfilHeader;
import org.bonn.model.dto.KundeDTO;

public class KundeProfilView extends Panel implements View {
    KundeDTO kundeDTO = (KundeDTO) UI.getCurrent().getSession().getAttribute("currentUser");

    FormLayout form = new FormLayout();
    ProfilHeader head = new ProfilHeader();

    public KundeProfilView() {

        addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        VerticalLayout root = new VerticalLayout();
        //root.setSizeFull();
        root.setSpacing(false);
        setContent(root);
        Responsive.makeResponsive(root);
        root.addComponent(head.buildHeader("Mein Profil"));
        Component content = buildContent();
        root.addComponent(content);
        root.setExpandRatio(content, 1);
    }

    private Component buildContent() {
        //form.setMargin(false);
        //form.setWidthFull();
        form.setWidth("500");
        form.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);

        Label section = new Label("Persönliche Informationen");
        section.addStyleName(ValoTheme.LABEL_H2);
        section.addStyleName(ValoTheme.LABEL_COLORED);
        form.addComponent(section);

        Binder<KundeDTO> binder = new Binder<>();

        TextField vorname = new TextField("Vorname");
        binder.forField(vorname)
                .asRequired("Bitte geben sie ihren Vornamen an!")
                .bind(KundeDTO::getVorname, KundeDTO::setVorname);
        vorname.setId("firstname");
        vorname.setWidth("50%");
        form.addComponent(vorname);

        TextField nachname = new TextField("Nachname");
        binder.forField(nachname)
                .asRequired("Bitte geben sie ihren Nachnamen an!")
                .bind(KundeDTO::getNachname, KundeDTO::setNachname);
        nachname.setId("lastname");
        nachname.setWidth("50%");
        form.addComponent(nachname);

        Label section2 = new Label("Kontakt");
        section2.addStyleName(ValoTheme.LABEL_H2);
        section2.addStyleName(ValoTheme.LABEL_COLORED);
        form.addComponent(section2);


        //User Stuff Email and Password

        TextField email = new TextField("Email");
        binder.forField(email)
                .withValidator(new EmailValidator("Die Emailadresse scheint falsch zu sein!"))
                .asRequired()
                .bind(KundeDTO::getEmail, KundeDTO::setEmail);
        email.setId("email");
        email.setWidth("50%");
        form.addComponent(email);

        //TODO Passwort

        HorizontalLayout footer = new HorizontalLayout();
        footer.setMargin(new MarginInfo(true, false, true, false));
        footer.setSpacing(true);
        footer.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        footer.setId("footer");
        form.addComponent(footer);

        // Read all Binder
        binder.readBean(kundeDTO);

        //Abbruch Button
        Button abbruchButton = new Button("Abbruch", (Button.ClickListener) event2 -> {
            setFormReadOnly(true);
            form.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
            Page.getCurrent().reload();
        });
        abbruchButton.setId("abbruchButton");
        abbruchButton.setVisible(false);
        footer.addComponent(abbruchButton);

        setFormReadOnly(true);
        return form;
    }


    private void setFormReadOnly(boolean bool) {
        for (Component c : form) {
            if (c instanceof AbstractComponent && !"footer".equals(c.getId())) {
                c.setEnabled(!bool);
            }
        }
    }
}
