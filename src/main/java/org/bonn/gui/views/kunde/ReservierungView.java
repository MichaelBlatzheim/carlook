package org.bonn.gui.views.kunde;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.gui.views.ProfilHeader;
import org.bonn.model.dto.KundeDTO;
import org.bonn.model.entities.Auto;
import org.bonn.process.control.ReservierungController;

import java.sql.SQLException;
import java.util.Set;


public class ReservierungView extends VerticalLayout implements View {


    KundeDTO kundeDTO = (KundeDTO) UI.getCurrent().getSession().getAttribute("currentUser");

    ReservierungController reservierungController = new ReservierungController();
    VerticalLayout vl = new VerticalLayout();
    ProfilHeader header = new ProfilHeader();

    public ReservierungView() throws SQLException {
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        addComponent(header.buildHeader("Reservierungen"));

        Component content = buildcontent();
        addComponent(content);
        setExpandRatio(content, 1);
    }

    private Component buildcontent() throws SQLException {
        Grid<Auto> grid2 = new Grid<>();

        Set<Auto> reservierteAutos = reservierungController.getReservierteAutos(kundeDTO.getKunde_id());

        ListDataProvider<Auto> dataProvider = DataProvider.ofCollection(reservierteAutos);
        grid2.setDataProvider(dataProvider);
        grid2.addColumn(Auto::getFin_id).setCaption("FIN");
        grid2.addColumn(Auto::getMarke).setCaption("Marke");
        grid2.addColumn(Auto::getBeschreibung).setCaption("Beschreibung");
        grid2.addColumn(Auto::getFarbe).setCaption("Farbe");
        grid2.addColumn(Auto::getBaujahr).setCaption("Baujahr");
        grid2.addColumn(Auto::getLeistung).setCaption("Leistung (PS)");
        grid2.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid2.setSizeFull();
        grid2.asSingleSelect();

        vl.addComponent(grid2);
        return vl;
    }
}
