package org.bonn.gui.views;

import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import org.bonn.gui.views.kunde.KundeProfilView;
import org.bonn.gui.views.kunde.KundeSucheView;
import org.bonn.gui.views.kunde.ReservierungView;


public enum KundeViewType {
    PROFIL("profil", KundeProfilView.class, FontAwesome.HOME, true),
    SUCHE("suche", KundeSucheView.class, FontAwesome.SEARCH, true),
    RESERVIERUNG("reservierungen", ReservierungView.class, FontAwesome.LIST, true);

    private final String viewName;
    private final Class<? extends View> viewClass;
    private final Resource icon;
    private final boolean stateful;

    KundeViewType(final String viewName,
                  final Class<? extends View> viewClass, final Resource icon,
                  final boolean stateful) {
        this.viewName = viewName;
        this.viewClass = viewClass;
        this.icon = icon;
        this.stateful = stateful;
    }

    public static KundeViewType getByViewName(final String viewName) {
        KundeViewType result = null;
        for (KundeViewType viewType : KundeViewType.values()) {
            if (viewType.getViewName().equals(viewName)) {
                result = viewType;
                break;
            }
        }
        return result;
    }

    public boolean isStateful() {
        return stateful;
    }

    public String getViewName() {
        return viewName;
    }

    public Class<? extends View> getViewClass() {
        return viewClass;
    }

    public Resource getIcon() {
        return icon;
    }
}
