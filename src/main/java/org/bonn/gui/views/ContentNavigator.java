package org.bonn.gui.views;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.bonn.process.control.LoginController;

public class ContentNavigator extends Navigator {
    private static final KundeViewType ERROR_VIEW = KundeViewType.PROFIL;
    private ViewProvider errorViewProvider;

    public ContentNavigator(ComponentContainer content) {
        super(UI.getCurrent(), content);
        initViewChangeListener();
        initViewProviders();
    }

    private void initViewChangeListener() {
        addViewChangeListener(new ViewChangeListener() {
            public boolean beforeViewChange(final ViewChangeEvent event) {
                if (event.getNewView() instanceof MainView && (UI.getCurrent().getSession().getAttribute("currentUser") == null)) {
                    Notification.show("Permission denied", Notification.Type.ERROR_MESSAGE);
                    return false;
                } else {
                    return true;
                }
            }

            public void afterViewChange(final ViewChangeEvent event) {
                KundeViewType.getByViewName(event.getViewName());
            }
        });
    }

    private void initViewProviders() {
        LoginController loginController = new LoginController();
        if (loginController.isKunde()) {
            kundenProvider();
        }

    }

    private void kundenProvider() {
        for (final KundeViewType viewType : KundeViewType.values()) {
            ViewProvider viewProvider = new ClassBasedViewProvider(
                    viewType.getViewName(), viewType.getViewClass()) {

                private View cachedInstance;

                @Override
                public View getView(final String viewName) {
                    View result = null;
                    if (viewType.getViewName().equals(viewName)) {
                        if (viewType.isStateful()) {
                            // Stateful views get lazily instantiated
                            if (cachedInstance == null) {
                                cachedInstance = super.getView(viewType
                                        .getViewName());
                            }
                            result = cachedInstance;
                        } else {
                            // Non-stateful views get instantiated every time
                            // they're navigated to
                            result = super.getView(viewType.getViewName());
                        }
                    }
                    return result;
                }
            };
            if (viewType == ContentNavigator.ERROR_VIEW) {
                errorViewProvider = viewProvider;
            }
            addProvider(viewProvider);
        }
        setErrorProvider(new ViewProvider() {
            @Override
            public String getViewName(final String viewAndParameters) {
                return ContentNavigator.ERROR_VIEW.getViewName();
            }

            @Override
            public View getView(final String viewName) {
                return errorViewProvider.getView(ContentNavigator.ERROR_VIEW.getViewName());
            }
        });

    }
}