package org.bonn.gui.views;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;

public class ProfilHeader {
    public Component buildHeader(String ueberschrift) {
        HorizontalLayout header = new HorizontalLayout();
        header.addStyleName("viewheader");
        Label titleLabel = new Label(ueberschrift);
        titleLabel.setSizeUndefined();
        titleLabel.addStyleName(ValoTheme.LABEL_H1);
        titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
        header.addComponent(titleLabel);
        return header;
    }
}
