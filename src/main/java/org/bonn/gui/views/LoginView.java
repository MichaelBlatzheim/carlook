package org.bonn.gui.views;


import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.bonn.exception.NoSuchUserOrPasswort;
import org.bonn.process.control.LoginController;


public class LoginView extends VerticalLayout {
    private final LoginController loginController = new LoginController();

    public LoginView() {
        addStyleName("loginview");
        setSizeFull();
        setMargin(false);
        setSpacing(false);

        Component loginForm = buildLoginForm();
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
    }


    private Component buildLoginForm() {

        final VerticalLayout loginPanel = new VerticalLayout();
        loginPanel.setSizeUndefined();
        loginPanel.setMargin(false);
        Responsive.makeResponsive(loginPanel);
        loginPanel.addStyleName("login-panel");


        loginPanel.addComponent(buildLabels());
        loginPanel.addComponent(buildFields());
        loginPanel.addComponent(new CheckBox("Remember me", true));
        return loginPanel;
    }

    private Component buildFields() {
        HorizontalLayout fields = new HorizontalLayout();
        fields.addStyleName("fields");

        final TextField username = new TextField("E-Mail");
        username.setIcon(FontAwesome.USER);
        username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        final PasswordField password = new PasswordField("Passwort");
        password.setIcon(FontAwesome.LOCK);
        password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        final Button signin = new Button("Einloggen", (Button.ClickListener) event -> {
            try {
                loginController.login(username.getValue(), password.getValue());
            } catch (NoSuchUserOrPasswort ex) {
                Notification.show("User: " + username.getValue() + " mit dem eingegebenen Passwort ist nicht hinterlegt.", Notification.Type.ERROR_MESSAGE);
                username.setValue("");
                password.setValue("");
            } catch (NullPointerException ex) {
                username.setValue("");
                password.setValue("");
            }

        });

        final Button buttonAbbrechen = new Button("Abbrechen", (Button.ClickListener) event ->
            UI.getCurrent().getPage().reload());


        buttonAbbrechen.addStyleName(ValoTheme.BUTTON_QUIET);
        signin.addStyleName(ValoTheme.BUTTON_PRIMARY);
        signin.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        signin.focus();

        fields.addComponents(username, password, signin, buttonAbbrechen);
        fields.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);
        fields.setComponentAlignment(buttonAbbrechen, Alignment.BOTTOM_RIGHT);
        return fields;
    }

    private Component buildLabels() {
        CssLayout labels = new CssLayout();
        labels.addStyleName("labels");

        Label welcome = new Label("Anmelden");
        welcome.setSizeUndefined();
        welcome.addStyleName(ValoTheme.LABEL_H4);
        welcome.addStyleName(ValoTheme.LABEL_COLORED);
        labels.addComponent(welcome);

        return labels;
    }
}
