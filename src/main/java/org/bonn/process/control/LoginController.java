package org.bonn.process.control;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.bonn.exception.NoSuchUserOrPasswort;
import org.bonn.model.dao.KundeDAO;
import org.bonn.model.dto.KundeDTO;
import org.bonn.model.entities.Kunde;

import java.sql.SQLException;

public class LoginController {
    KundeDAO kundeDAO = new KundeDAO();

    public void login(String username, String password) throws NoSuchUserOrPasswort, NullPointerException {
        Kunde kundeLogin;
        try {
            kundeLogin = kundeDAO.getUserByEmailAndPass(username, password);
        } catch (NullPointerException | SQLException e) {
            Notification.show("Folgender Fehler ist aufgetreten\n" + e.getMessage(), Notification.Type.ERROR_MESSAGE);
            throw new NoSuchUserOrPasswort();
        }
        if (kundeLogin != null) {
            KundeDAO kundeDAO = new KundeDAO();
            try {
                KundeDTO kundeDTO = new KundeDTO(kundeDAO.getStudentByUserID(kundeLogin.getId()));
                kundeDTO.setEmail(kundeLogin.getEmail());
                kundeDTO.setPasswort(kundeLogin.getPasswort());
                UI.getCurrent().getSession().setAttribute("currentUser", kundeDTO);

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            Page.getCurrent().reload();

        } else {
            throw new NoSuchUserOrPasswort();
        }
    }

    public void logout() {
        VaadinSession.getCurrent().close();
        UI.getCurrent().getNavigator().navigateTo("LoginView");
        Page.getCurrent().reload();

    }

    public boolean isKunde() {
        return UI.getCurrent().getSession().getAttribute("currentUser") instanceof KundeDTO;
    }

}