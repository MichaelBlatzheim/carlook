package org.bonn.process.control;


import org.bonn.model.dao.AutoSucheDAO;
import org.bonn.model.dto.AutoDTO;
import org.bonn.model.entities.Auto;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class AutoController {
    AutoSucheDAO dao = new AutoSucheDAO();

    public Set<Auto> getAllGesuche() {
        try {
            return dao.getAll();
        } catch (SQLException throwables) {
            System.out.println("Error mit Datenbank");
            return null;
        }
    }


    public boolean updateGesuch(AutoDTO autoDTO) {
        Auto auto = new Auto(autoDTO);
        try {
            return dao.update(auto);
        } catch (SQLException throwables) {
            System.out.println("Error mit Datenbank");
            return false;
        }
    }

    public AutoDTO read(int gesuchsId) {
        try {
            return new AutoDTO(dao.read(gesuchsId));
        } catch (SQLException throwables) {
            System.out.println("Error mit Datenbank");
            return null;
        }
    }

    public void delete(AutoDTO autoDTO) {
        Auto auto = new Auto(autoDTO);
        try {
            dao.delete(auto);
        } catch (SQLException throwables) {
            System.out.println("Error mit Datenbank");
        }
    }
}
