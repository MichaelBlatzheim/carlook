package org.bonn.process.control;


import org.bonn.model.dao.ReservierungDAO;
import org.bonn.model.dto.KundeDTO;
import org.bonn.model.entities.Auto;
import org.bonn.model.entities.Reservierung;

import java.sql.SQLException;
import java.util.Set;

public class ReservierungController {


    public boolean autoReservieren(Auto auto, KundeDTO kundeDTO){

        ReservierungDAO reservierungDAO = new ReservierungDAO();

        Reservierung reservierung = new Reservierung();
        reservierung.setUser_id(kundeDTO.getKunde_id());
        reservierung.setFin_id(auto.getFin_id());

        if (reservierungDAO.getRsByIDs(auto.getFin_id(), kundeDTO.getKunde_id()) != null) {
            return false;
        }

        try {
            return reservierungDAO.create(reservierung);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }


    }

    public Set<Auto> getReservierteAutos(int user_id) throws SQLException {

        ReservierungDAO reservierungDAO = new ReservierungDAO();

        return reservierungDAO.getReservierteAutos(user_id);


    }
}