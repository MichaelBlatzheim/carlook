package org.bonn.process.control;


import org.bonn.exception.EmailVorhanden;
import org.bonn.model.dao.KundeDAO;
import org.bonn.model.dto.KundeDTO;
import org.bonn.model.entities.Kunde;

import java.sql.SQLException;

public class RegisterController {


    public boolean registerKunde(KundeDTO kundeDTO) throws SQLException, EmailVorhanden {

        KundeDAO kundeDAO = new KundeDAO();
        Kunde kunde = new Kunde(kundeDTO);


        if (kundeDAO.getByEmail(kunde.getEmail()) != null) {
            throw new EmailVorhanden();
        }

        try {
            return kundeDAO.create(kunde);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }


}
