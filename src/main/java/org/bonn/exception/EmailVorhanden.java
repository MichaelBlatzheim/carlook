package org.bonn.exception;

public class EmailVorhanden extends Exception {
    public EmailVorhanden() {
        super("E-Mail Adresse bereits vorhanden");
    }
}
