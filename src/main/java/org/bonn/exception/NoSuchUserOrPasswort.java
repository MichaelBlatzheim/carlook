package org.bonn.exception;

/**
 * Einfach Exception wenn Password oder Email invalide ist.
 *
 * @author rjourd2s
 */

public class NoSuchUserOrPasswort extends Exception {

    public NoSuchUserOrPasswort() {
        super("User nicht vorhanden");
    }
}