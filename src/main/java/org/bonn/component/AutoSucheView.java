package org.bonn.component;

import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import org.bonn.model.dto.KundeDTO;
import org.bonn.model.entities.Auto;
import org.bonn.process.control.ReservierungController;

import java.sql.SQLException;

public class AutoSucheView extends Window {

    private final ReservierungController reservierungController = new ReservierungController();
    KundeDTO kundeDTO = (KundeDTO) UI.getCurrent().getSession().getAttribute("currentUser");

    public AutoSucheView(Auto auto) {
        setCaption("Ausgewähltes Auto: " + auto.getMarke());
        setModal(true);
        Responsive.makeResponsive(this);
        setClosable(true);
        setWidth("40%");
        setHeight("80%");
        setDraggable(true);
        final VerticalLayout layout = new VerticalLayout();

        Label labelLeistung = new Label(auto.getLeistung() + "", ContentMode.HTML);
        labelLeistung.setCaption("Leistung in PS:");
        labelLeistung.setId("leistung");

        Label labelFarbe = new Label(auto.getFarbe(), ContentMode.HTML);
        labelFarbe.setCaption("Farbe:");
        labelFarbe.setId("farbe");


        Label labelBeschreibung = new Label(auto.getBeschreibung(), ContentMode.HTML);
        labelBeschreibung.setCaption("Beschreibung:");
        labelBeschreibung.setId("beschreibung");
        labelBeschreibung.setWidth("80%");
        //beschreibung.setSizeFull();

        Label labelBaujahr = new Label(auto.getBaujahr() + "");
        labelBaujahr.setCaption("Baujahr: ");
        labelBaujahr.setId("datum");


        final Button autoReservieren = new Button("Auto reservieren", (Button.ClickListener) event -> {

            try {
                if (reservierungController.autoReservieren(auto, kundeDTO)) {
                    UI.getCurrent().addWindow(new AutoSucheView(auto));
                    Notification success = new Notification("Ihre Auto wurde reserviert!");
                    success.setDelayMsec(6000);
                    success.setStyleName("bar success small");
                    success.setPosition(Position.BOTTOM_CENTER);
                    success.show(Page.getCurrent());
                } else {
                    Notification.show("Dieses Auto wurde bereits von Ihnen reserviert", Notification.Type.ERROR_MESSAGE);
                }
            } catch (Exception throwables) {
                throwables.printStackTrace();
            }
        });


        layout.addComponent(labelBeschreibung);
        layout.addComponent(labelFarbe);
        layout.addComponent(labelBaujahr);
        layout.addComponent(labelLeistung);
        layout.addComponent(autoReservieren);
        setContent(layout);


    }
}
