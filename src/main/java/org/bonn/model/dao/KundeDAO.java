package org.bonn.model.dao;

import org.bonn.model.entities.Kunde;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public class KundeDAO extends AbstractDAO<Kunde> {
    public KundeDAO() {
        super("carlook_mblatz2s.kunde", "user_id");
    }

    @Override
    public boolean create(Kunde kunde) throws SQLException {
        return createps(
                kunde,
                this.getPreparedStatement(String.format("INSERT INTO %s VALUES (default,?,?,?,?)", super.tableName))
        );
    }

    public Kunde getByEmail(String email) throws SQLException {
        return readResults(this.getStatement().executeQuery(String.format("SELECT * FROM %s WHERE email='%s'", tableName, email)));
    }

    public Kunde getUserByEmailAndPass(String email, String passwort) throws SQLException {

        ResultSet resultSet = this.getStatement().executeQuery(String.format("SELECT user_id FROM %s WHERE email='%s' AND passwort='%s'", tableName, email, passwort));
        if (resultSet.next()) {
            return read(resultSet.getInt(1));
        }
        return null;
    }

    @Override
    public Kunde read(int kunde_id) throws SQLException {
        return readResults(super.getRsByID(kunde_id));
    }

    @Override
    public boolean update(Kunde kunde) throws SQLException {
        return createps(kunde, this.getPreparedStatement(String.format("UPDATE %s SET user_id=?, vorname=?, nachname=?, email=? , passwort=? WHERE user_id='%d'", tableName, kunde.getId())));
    }

    @Override
    public boolean delete(Kunde kunde) throws SQLException {
        return this.getStatement().executeUpdate(String.format("DELETE FROM %s WHERE %s=%d", tableName, primaryKey, kunde.getKunde_id())) == 1;
    }

    @Override
    public Set<Kunde> getAllByID(int id){
        return null;
    }

    public Kunde getStudentByUserID(int kunde_id) throws SQLException {
        return readResults(this.getStatement().executeQuery(String.format("SELECT * FROM %s WHERE user_id=%d", tableName, kunde_id)));
    }

    private boolean createps(Kunde kunde, PreparedStatement ps) throws SQLException {
        ps.setString(1, kunde.getVorname());
        ps.setString(2, kunde.getNachname());
        ps.setString(3, kunde.getEmail());
        ps.setString(4, kunde.getPasswort());

        ps.executeUpdate();
        Kunde kunde1 = getUserByEmailAndPass(kunde.getEmail(), kunde.getPasswort());
        kunde.setKunde_id(kunde1.getKunde_id());

        return kunde1.getKunde_id() != 0;
    }

    private Kunde readResults(ResultSet rs) throws SQLException {
        if (!rs.next()) {
            return null;
        }

        Kunde kunde = new Kunde();

        kunde.setKunde_id(rs.getInt(1));
        kunde.setId(rs.getInt(1));
        kunde.setVorname(rs.getString(2));
        kunde.setNachname(rs.getString(3));
        kunde.setEmail(rs.getString(4));
        kunde.setPasswort(rs.getString(5));


        return kunde;
    }

}
