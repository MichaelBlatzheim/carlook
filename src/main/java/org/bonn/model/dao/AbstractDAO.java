package org.bonn.model.dao;

import org.bonn.db.JDBCConnection;
import org.bonn.model.entities.AbstractEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDAO<T extends AbstractEntity> implements IGenericDAO<T> {
    String primaryKey;
    String primaryKey2;
    String tableName;

    AbstractDAO(String tableName, String primaryKey) {
        this.tableName = tableName;
        this.primaryKey = primaryKey;
    }

    AbstractDAO(String tableName, String primaryKey, String primaryKey2) {
        this.tableName = tableName;
        this.primaryKey = primaryKey;
        this.primaryKey2 = primaryKey2;
    }

    protected PreparedStatement getPreparedStatement(String sql) {
        PreparedStatement statement = null;

        try {
            statement = JDBCConnection.getInstance().getPreparedStatement(sql);
        } catch (Exception ex) {
            //TODO
        }
        return statement;
    }

    protected Statement getStatement() {
        Statement statement = null;

        try {
            statement = JDBCConnection.getInstance().getStatement();
        } catch (Exception ex) {
            //TODO
        }
        return statement;
    }


    @Override
    public boolean delete(T entity) throws SQLException {
        return getStatement().executeUpdate(String.format("DELETE FROM %s WHERE %s=%d", tableName, primaryKey, entity.getId())) == 1;
    }

    @Override
    public ResultSet getRsByID(int id) throws SQLException {
        return getStatement().executeQuery(String.format("SELECT * FROM %s WHERE %s='%s'", tableName, primaryKey, id));
    }

    @Override
    public ResultSet getRsByIDs(int fin_id, int id) throws SQLException {
        return getStatement().executeQuery(String.format("SELECT * FROM %s WHERE %s='%s' AND %s='%s'", tableName, primaryKey, primaryKey2, fin_id, id));
    }

}
