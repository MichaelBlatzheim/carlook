package org.bonn.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public interface IGenericDAO<T> {
    // CRUD Methods

    boolean create(T entity) throws SQLException;

    T read(int id) throws SQLException;

    boolean update(T entity) throws SQLException;

    boolean delete(T entity) throws SQLException;

    ResultSet getRsByID(int id) throws SQLException;

    Set<T> getAllByID(int id) throws SQLException;

    ResultSet getRsByIDs(int fin_id, int id) throws SQLException;
}