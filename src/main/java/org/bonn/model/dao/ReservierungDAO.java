package org.bonn.model.dao;

import org.bonn.model.entities.Auto;
import org.bonn.model.entities.Reservierung;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class ReservierungDAO extends AbstractDAO<Reservierung> {

    public ReservierungDAO() {
        super("carlook_mblatz2s.reservierung", "user_id", "fin_id");
    }

    @Override
    public boolean create(Reservierung reservierung) throws SQLException {
        return createps(
                reservierung,
                this.getPreparedStatement(String.format("INSERT INTO %s VALUES (?,?)", super.tableName))
        );
    }

    @Override
    public Reservierung read(int id){
        return null;
    }

    public Set<Auto> getReservierteAutos(int id) throws SQLException {

        ResultSet reservierteAutosVonKunde = getStatement().executeQuery(String.format("SELECT fin_id FROM %s WHERE user_id='%s'", tableName, id));

        AutoSucheDAO autoSucheDAO = new AutoSucheDAO();
        Set<Auto> autos = new HashSet<>();

        while (reservierteAutosVonKunde.next()) {
            autos.add(autoSucheDAO.read(reservierteAutosVonKunde.getInt(1)));
        }

        return autos;

    }

    @Override
    public ResultSet getRsByIDs(int fin_id, int id){
        return null;

    }


    @Override
    public boolean update(Reservierung reservierung) throws SQLException {
        return createps(reservierung, this.getPreparedStatement(String.format("UPDATE %s SET user_id=?, vorname=?, nachname=?, email=? , passwort=? WHERE user_id='%d'", tableName, reservierung.getFin_id())));
    }

    @Override
    public boolean delete(Reservierung reservierung) throws SQLException {
        return this.getStatement().executeUpdate(String.format("DELETE FROM %s WHERE %s=%d", tableName, primaryKey, reservierung.getFin_id())) == 1;
    }

    @Override
    public Set<Reservierung> getAllByID(int id){
        return null;
    }

    private boolean createps(Reservierung reservierung, PreparedStatement ps) throws SQLException {
        ps.setInt(1, reservierung.getFin_id());
        ps.setInt(2, reservierung.getUser_id());


        return ps.executeUpdate() == 1;
    }

    private Reservierung readResults(ResultSet rs) throws SQLException {
        if (!rs.next()) {
            return null;
        }

        Reservierung reservierung = new Reservierung();

        reservierung.setFin_id(rs.getInt(1));
        reservierung.setUser_id(rs.getInt(2));


        return reservierung;
    }

}
