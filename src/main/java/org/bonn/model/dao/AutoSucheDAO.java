package org.bonn.model.dao;

import org.bonn.model.dto.AutoDTO;
import org.bonn.model.entities.Auto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class AutoSucheDAO extends AbstractDAO<Auto> {

    public AutoSucheDAO() {
        super("carlook_mblatz2s.auto", "fin_id");
    }

    @Override
    public boolean create(Auto auto) throws SQLException {
        return createPs(
                auto,
                this.getPreparedStatement((String.format("INSERT INTO %s VALUES (default,?,?,?,?,?)", tableName)))
        );
    }

    @Override
    public Auto read(int id) throws SQLException {
        return readResults(super.getRsByID(id));
    }

    @Override
    public boolean update(Auto auto) throws SQLException {
        return createPs(
                auto,
                getPreparedStatement(String.format("UPDATE %s SET " +
                        "titel=?, beschreibung=?, erstellungsdatum=?, art_studentenjob=?, art_praktikum=?, art_masterarbeit=?, unternehmens_id=? " +
                        "WHERE gesuch_id='%d'", tableName, auto.getId())));
    }


    @Override
    public Set<Auto> getAllByID(int user_id) throws SQLException {
        Set<Auto> gesuche = new HashSet<>();
        ResultSet rs = getStatement().executeQuery(String.format("SELECT * FROM %s WHERE fin_id ='%d'", tableName, user_id));

        while (rs.next()) {
            Auto auto = readResultSet(rs);
            gesuche.add(auto);
        }
        return gesuche;
    }

    /*
    public Auto getGesuchbyId(int unternehmensId) throws SQLException {
        return readResults(this.getStatement().executeQuery(String.format("SELECT * FROM %s WHERE unternehmens_id='%d'", tableName, unternehmensId)));
    }

     */

    public Set<Auto> getAll() throws SQLException {
        Set<Auto> gesuche = new HashSet<>();
        ResultSet rs = this.getStatement().executeQuery(String.format("SELECT * FROM %s", tableName));

        while (rs.next()) {
            Auto auto = readResultSet(rs);
            gesuche.add(auto);
        }
        return gesuche;
    }


    private boolean createPs(Auto auto, PreparedStatement ps) throws SQLException {
        ps.setString(1, auto.getMarke());
        ps.setInt(2, auto.getBaujahr());
        ps.setInt(3, auto.getLeistung());
        ps.setString(4, auto.getFarbe());
        ps.setString(5, auto.getBeschreibung());

        ps.executeUpdate();
        ResultSet rs = ps.getResultSet();
        auto.setFin_id(rs.getInt(1));

        return ps.executeUpdate() == 1;
    }

    private Auto readResults(ResultSet rs) throws SQLException {
        if (!rs.next()) {
            return null;
        }

        Auto auto = new Auto();

        auto.setFin_id(rs.getInt(1));
        auto.setMarke(rs.getString(2));
        auto.setBaujahr(rs.getInt(3));
        auto.setLeistung(rs.getInt(4));
        auto.setFarbe(rs.getString(5));
        auto.setBeschreibung(rs.getString(6));


        return auto;

    }

    private Auto readResultSet(ResultSet rs) throws SQLException {
        Auto auto = new Auto();
        auto.setFin_id(rs.getInt("fin_id"));
        auto.setMarke(rs.getString("marke"));
        auto.setBaujahr(rs.getInt("baujahr"));
        auto.setLeistung(rs.getInt("leistung"));
        auto.setFarbe(rs.getString("farbe"));
        auto.setBeschreibung(rs.getString("beschreibung"));
        return auto;
    }
}
