package org.bonn.model.entities;

import org.bonn.model.dto.AutoDTO;

public class Auto extends AbstractEntity {

    private int fin_id;
    private String marke;
    private String beschreibung;
    private int baujahr;
    private int leistung;
    private String farbe;

    public Auto() {
    }

    public Auto(AutoDTO autoDTO) {
        setFin_id(autoDTO.getFin_id());
        setBaujahr(autoDTO.getBaujahr());
        setBeschreibung(autoDTO.getBeschreibung());
        setFarbe(autoDTO.getFarbe());
        setLeistung(autoDTO.getLeistung());
        setMarke(autoDTO.getMarke());

    }

    public int getFin_id() {
        return fin_id;
    }

    public void setFin_id(int fin_id) {
        this.fin_id = fin_id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public int getLeistung() {
        return leistung;
    }

    public void setLeistung(int leistung) {
        this.leistung = leistung;
    }

    public String getFarbe() {
        return farbe;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }


}
