package org.bonn.model.entities;

import org.bonn.model.dto.KundeDTO;

public class Kunde extends AbstractEntity {

    private int kunde_id;
    private String vorname;
    private String nachname;
    private String email;
    private String passwort;

    public Kunde() {

    }

    public Kunde(KundeDTO kundeDTO) {
        setVorname(kundeDTO.getVorname());
        setKunde_id(kundeDTO.getKunde_id());
        setNachname(kundeDTO.getNachname());
        setEmail(kundeDTO.getEmail());
        setPasswort(kundeDTO.getPasswort());
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public int getKunde_id() {
        return kunde_id;
    }

    public void setKunde_id(int kunde_id) {
        this.kunde_id = kunde_id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
