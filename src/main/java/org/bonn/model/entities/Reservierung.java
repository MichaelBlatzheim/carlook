package org.bonn.model.entities;

import org.bonn.model.dto.ReservierungDTO;

public class Reservierung extends AbstractEntity {
    private int fin_id;
    private int user_id;

    public Reservierung() {

    }

    public Reservierung(ReservierungDTO reservierungDTO) {
        setFin_id(reservierungDTO.getFin_id());
        setUser_id(reservierungDTO.getUser_id());
    }

    public int getFin_id() {
        return fin_id;
    }

    public void setFin_id(int fin_id) {
        this.fin_id = fin_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
