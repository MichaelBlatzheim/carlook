package org.bonn.model.entities;

public abstract class AbstractEntity {

    private int id;

    final public int getId() {
        return this.id;
    }

    final public void setId(int id) {
        this.id = id;
    }
}
