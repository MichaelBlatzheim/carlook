package org.bonn.model.dto;

import org.bonn.model.entities.Reservierung;

public class ReservierungDTO {

    private int fin_id;
    private int user_id;

    public ReservierungDTO() {

    }

    public ReservierungDTO(Reservierung reservierung) {
        setFin_id(reservierung.getFin_id());
        setUser_id(reservierung.getUser_id());
    }

    public int getFin_id() {
        return fin_id;
    }

    public void setFin_id(int fin_id) {
        this.fin_id = fin_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
