package org.bonn.model.dto;

import org.bonn.model.entities.Auto;

public class AutoDTO {

    private int fin_id;
    private String marke;
    private String beschreibung;
    private int baujahr;
    private int leistung;
    private String farbe;


    public AutoDTO() {

    }

    public AutoDTO(Auto auto) {
        setFin_id(auto.getFin_id());
        setMarke(auto.getMarke());
        setBeschreibung(auto.getBeschreibung());
        setBaujahr(auto.getBaujahr());
        setLeistung(auto.getLeistung());
        setFarbe(auto.getFarbe());
    }

    public int getFin_id() {
        return fin_id;
    }

    public void setFin_id(int fin_id) {
        this.fin_id = fin_id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public int getLeistung() {
        return leistung;
    }

    public void setLeistung(int leistung) {
        this.leistung = leistung;
    }

    public String getFarbe() {
        return farbe;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }


}
