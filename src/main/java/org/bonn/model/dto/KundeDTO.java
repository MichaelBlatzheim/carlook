package org.bonn.model.dto;

import org.bonn.model.entities.Kunde;

public class KundeDTO {

    private int kunde_id;
    private String vorname;
    private String nachname;
    private String email;
    private String passwort;

    public KundeDTO() {

    }

    public KundeDTO(Kunde kunde) {
        setKunde_id(kunde.getKunde_id());
        setVorname(kunde.getVorname());
        setNachname(kunde.getNachname());
    }

    public int getKunde_id() {
        return kunde_id;
    }

    public void setKunde_id(int kunde_id) {
        this.kunde_id = kunde_id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }
}
